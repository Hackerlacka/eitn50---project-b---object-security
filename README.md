The BouncyCastle library must be imported into the project before the project can be run.

Run instructions:
1. Start the cache server (CacheServer/Main.java)
2. Start the server (server/Main.java)
3. Start the client (client/Main.java)