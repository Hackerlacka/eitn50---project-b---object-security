package CacheServer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;

public class Main {

    private final static int RECEIVE_PORT_CLIENT = 60000;
    private final static int SERVER_PORT = 33000;

    public final static int MAX_PACKET_SIZE = 64 -4;

    private static DatagramSocket clientSide;
    private static DatagramSocket serverSide;

    private static InetAddress clientIp;
    private static InetAddress serverIp;
    private static int clientPort;

    private static Monitor monitor;

    public static void main(String[] args) {
        System.out.println("Cache server starting...");

        try {
            clientSide = new DatagramSocket(RECEIVE_PORT_CLIENT);
            serverSide = new DatagramSocket();

            serverIp = InetAddress.getByName("localhost");

            //diffie hellman
            byte[] baseAndPrime = receive(clientSide);
            send(baseAndPrime, SERVER_PORT, serverIp, serverSide);

            byte[] part1client = receive(clientSide);
            send(part1client, SERVER_PORT, serverIp, serverSide);

            byte[] part1server = receive(serverSide);
            send(part1server, clientPort, clientIp, clientSide);

            monitor = new Monitor();
            new ReceiverThreadFromClient(monitor).start();
            new SenderThreadToServer(monitor).start();
            new ReceiverThreadFromServer(monitor).start();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception occured... Shutting down");
            System.exit(0);
        }



    }

    public static void send(byte[] data, int port, InetAddress IPAddress, DatagramSocket socket) throws IOException {
        DatagramPacket sendPacket = new DatagramPacket(data, data.length, IPAddress, port);
        socket.send(sendPacket);

        if(socket == clientSide) {
            System.out.println("Sent a packet from the client side: " + new String(data));
            System.out.println();

        } else if (socket == serverSide) {
            System.out.println("Sent a packet from the server side: " + new String(data));
            System.out.println();
        }

    }

    public static byte[] receive(DatagramSocket socket) throws IOException {
        byte[] data = new byte[MAX_PACKET_SIZE];
        DatagramPacket receivePacket = new DatagramPacket(data, data.length);
        socket.receive(receivePacket);

        if(socket == clientSide) {
            clientIp = receivePacket.getAddress();
            clientPort = receivePacket.getPort();
            System.out.println("Received a packet from the client side: " + new String(data));
            System.out.println();
        } else if (socket == serverSide) {
            System.out.println("Received a packet from the server side: " + new String(data));
            System.out.println();
            if(monitor != null) {
                monitor.pollReceive(data);
            }
        }

        return byteSplitter(data, 0, receivePacket.getLength());
    }

    public static byte[] byteSplitter(byte[] data, int start, int end) {
        byte[] result = new byte[end - start];
        for(int i = 0; i < end - start; i++) {
            result[i] = data[start + i];
        }
        return result;
    }

    private static class ReceiverThreadFromClient extends Thread {

        private Monitor monitor;

        private ReceiverThreadFromClient(Monitor monitor) {
            this.monitor = monitor;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    monitor.addDataToSend(receive(clientSide));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private static class ReceiverThreadFromServer extends Thread {

        private Monitor monitor;

        private ReceiverThreadFromServer(Monitor monitor) {
            this.monitor = monitor;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    monitor.pollReceive(receive(serverSide));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private static class SenderThreadToServer extends Thread {

        private Monitor monitor;

        private SenderThreadToServer(Monitor monitor) {
            this.monitor = monitor;
        }

        @Override
        public void run() {
            while(true) {
                try {
                    send(monitor.getNextDataToSend(), SERVER_PORT, serverIp, serverSide);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static class Monitor {

        private ArrayList<byte[]> cache;
        private int indexDataSent = 0;

        public Monitor() {
            cache = new ArrayList<>();
        }


        public synchronized void addDataToSend(byte[] data) {
            System.out.println("Adding data to cache. Size: " + cache.size());
            cache.add(data);
            notifyAll();
        }

        public synchronized byte[] getNextDataToSend() {
            while(indexDataSent == cache.size()) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Removing data from cache. Size: " + cache.size());
            return cache.get(indexDataSent++);
        }

        public synchronized void pollReceive(byte[] pollData) {
            if(pollData == null) return;
            int index = -1;
            for(int i = 0; i < cache.size(); i++) {
                if(isSameArray(pollData, cache.get(i))) {
                    index = i;
                    System.out.println("POLL DATA MATCH FOUND");
                    break;
                }
            }

            if(index != -1) {
                for(int k = 0; k < index; k++) {
                    cache.remove(0);
                }
                indexDataSent = 1;
                notifyAll();
            }
        }

        private boolean isSameArray(byte[] a, byte[] b) {
            if(a.length != b.length) {
                return false;
            }
            for(int i = 0; i < a.length; i++) {
                if(a[i] != b[i]) {
                    return false;
                }
            }
            return true;
        }
    }
}
