package client;

import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.params.KeyParameter;
import server.Main;

public class Crypto {

    private AESEngine encrypt;
    private AESEngine decrypt;

    public void init(byte[] key) {
        encrypt = new AESEngine();
        encrypt.init(true, new KeyParameter(key));
        decrypt = new AESEngine();
        decrypt.init(false, new KeyParameter(key));
    }


    public byte[] encrypt(byte[] data) {
        byte[] unencryptedData = new byte[48];
        for(int i = 0; i < data.length; i++) {
            unencryptedData[i] = data[i];
        }

        byte[] encryptedData = new byte[48];
        for(int i = 0; i < client.Main.MAX_PAYLOAD_DATA_IN_BYTES / client.Main.AES_BLOCK_SIZE_IN_BYTES; i++) {
            encrypt.processBlock(unencryptedData, i * client.Main.AES_BLOCK_SIZE_IN_BYTES, encryptedData, i * client.Main.AES_BLOCK_SIZE_IN_BYTES);
        }

        return encryptedData;
    }

    public byte[] decrypt(byte[] data) {
        byte[] decryptedData = new byte[48];
        for(int i = 0; i < client.Main.MAX_PAYLOAD_DATA_IN_BYTES / client.Main.AES_BLOCK_SIZE_IN_BYTES; i++) {
            decrypt.processBlock(data, i * client.Main.AES_BLOCK_SIZE_IN_BYTES, decryptedData, i * client.Main.AES_BLOCK_SIZE_IN_BYTES);
        }
        return decryptedData;
    }

}
