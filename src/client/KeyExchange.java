package client;

import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.prng.ThreadedSeedGenerator;
import org.bouncycastle.math.Primes;
import org.bouncycastle.util.encoders.Hex;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class KeyExchange {
    private final int BASE_SIZE_IN_BYTES = 4;
    private final int PRIME_SIZE_IN_BYTES = 16;
    private final int SECRET_SIZE_IN_BYTES = 4;

    private byte[] symmetricKey;

    public KeyExchange() {

    }

    public void client() throws IOException {

        BigInteger base = generateBase();
        BigInteger prime = generatePrimeNumber();
        System.out.println("Base: " + base + "\nPrime: "+ prime);
        byte[] test = prime.toByteArray();
        // Send base and prime
        System.out.println("Sending base and prime");
        Main.send(combineByteArrays(base.toByteArray(), prime.toByteArray()));

        BigInteger secret = generateSecretInteger();
        System.out.println("Generated secret (a): " + secret);
        BigInteger part1 = part1Calculation(base, prime, secret);
        System.out.println("Sending A = (g^a mod p): " + part1);
        // Send part1
        Main.send(part1.toByteArray());

        // Receive other base
        BigInteger otherBase = new BigInteger(Main.receive());
        System.out.println("Receiving B = (g^b mod p): " + otherBase);

        BigInteger sharedSecret = part2Calculation(otherBase, prime, secret);
        System.out.println("Shared secret (B^a mod p): " + sharedSecret);

        symmetricKey = generateSymmetricKey(sharedSecret);
    }

    public byte[] getSymmetricKey(){
        return symmetricKey;
    }

    // g
    private BigInteger generateBase() {
        while(true) {
            SecureRandom secureRandom = new SecureRandom();
            byte[] container = new byte[BASE_SIZE_IN_BYTES];
            secureRandom.nextBytes(container);
            BigInteger bigInteger = new BigInteger(container);
            if(BigInteger.valueOf(65000).compareTo(bigInteger) < 0) {
                return bigInteger;
            }
        }
    }

    // p
    private BigInteger generatePrimeNumber() {
        Digest digest = new SHA256Digest();
        while(true) {
            byte[] seed = new ThreadedSeedGenerator().generateSeed(1000, false);
            Primes.STOutput prime = Primes.generateSTRandomPrime(digest, (PRIME_SIZE_IN_BYTES - 1) * 8, seed);
            if(BigInteger.valueOf(65000).compareTo(prime.getPrime()) < 0) {
                return prime.getPrime();
            }
        }
    }

    // a or b
    private BigInteger generateSecretInteger() {
        while(true) {
            SecureRandom secureRandom = new SecureRandom();
            byte[] container = new byte[SECRET_SIZE_IN_BYTES];
            secureRandom.nextBytes(container);
            BigInteger bigInteger = new BigInteger(container);
            if(BigInteger.valueOf(65000).compareTo(bigInteger) < 0) {
                return bigInteger;
            }
        }
    }

    // g^a mod p or g^b mod p returns A or B
    private BigInteger part1Calculation(BigInteger base, BigInteger prime, BigInteger secret) {
        return base.modPow(secret, prime);
    }

    // B^a mod p or A^b mod p returns shared secret
    private BigInteger part2Calculation(BigInteger otherBase, BigInteger prime, BigInteger secret) {
        return otherBase.modPow(secret, prime);
    }

    private byte[] generateSymmetricKey(BigInteger sharedSecret) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(sharedSecret.toByteArray());
            String sha256hex = new String(Hex.encode(hash));
            System.out.println("Generated symmetric key: " + sha256hex);
            return hash;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    private byte[] combineByteArrays(byte[]... arrays) {
        int size = 0;
        for(int i = 0; i < arrays.length; i++) {
            size+= arrays[i].length;
        }
        byte[] result = new byte[size];
        int index = 0;
        for(int i = 0; i < arrays.length; i++) {
            byte[] part = arrays[i];
            for(int k = 0; k < part.length; k++) {
                result[index] = part[k];
                index++;
            }
        }
        return result;
    }

}
