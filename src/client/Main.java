package client;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Security;

public class Main {
    private final static int CACHE_PORT = 60000;
    public final static int MAX_PACKET_SIZE = 64 - 4;
    public final static int AES_BLOCK_SIZE_IN_BYTES = 16;
    public final static int MAX_PAYLOAD_DATA_IN_BYTES = (MAX_PACKET_SIZE / AES_BLOCK_SIZE_IN_BYTES) * AES_BLOCK_SIZE_IN_BYTES;
    public final static int BYTES_SENT_INDICATOR_SIZE_IN_BYTES = 1;
    public final static int SEQUENCE_NUMBER_SIZE_IN_BYTES = 4;
    public final static int HASH_SIZE_IN_BYTES = 16;
    public final static int MAX_USER_DATA_IN_BYTES = MAX_PAYLOAD_DATA_IN_BYTES - BYTES_SENT_INDICATOR_SIZE_IN_BYTES - HASH_SIZE_IN_BYTES - SEQUENCE_NUMBER_SIZE_IN_BYTES;

    private static DatagramSocket clientSocket;
    private static InetAddress IPAddress;
    private static Crypto crypto;

    private static int sendSequenceNumber = 0;
    private static int receiveSequenceNumber = 0;



    public static void main(String[] args) {
        System.out.println("Client starting...");
        Security.addProvider(new BouncyCastleProvider());

        try {
            // Initialization
            clientSocket = new DatagramSocket();
            IPAddress = InetAddress.getByName("localhost");

            KeyExchange keyExchange = new KeyExchange();
            keyExchange.client();

            crypto = new Crypto();
            crypto.init(keyExchange.getSymmetricKey());

            byte[] msg = new String("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.").getBytes();

            System.out.println("Sending message \"" + new String(msg) + "\" to server");

            sendData(msg);


            clientSocket.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception occured... Shutting down");
            System.exit(0);
        }
    }

    /*
    Use this method to send actual/raw data
     */
    public static void sendData(byte[] data) throws Exception {
        int packets = (int) Math.ceil(data.length / ((double) MAX_USER_DATA_IN_BYTES));
        for(int i = 0; i < packets; i++) {
            Thread.sleep(3000);
            byte[] splittedData = null;
            if(i != packets - 1) {
                splittedData = byteSplitter(data, i * MAX_USER_DATA_IN_BYTES, (i+1) * MAX_USER_DATA_IN_BYTES);
            } else {
                splittedData = byteSplitter(data, i * MAX_USER_DATA_IN_BYTES, i * MAX_USER_DATA_IN_BYTES + data.length % MAX_USER_DATA_IN_BYTES);
            }

            byte[] numberOfBytesSent = new byte[BYTES_SENT_INDICATOR_SIZE_IN_BYTES];
            numberOfBytesSent[0] = (byte) (SEQUENCE_NUMBER_SIZE_IN_BYTES + splittedData.length + HASH_SIZE_IN_BYTES);
            byte[] sequenceNbr = ByteBuffer.allocate(4).putInt(sendSequenceNumber).array();
            byte[] hash = hashData(combineByteArrays(numberOfBytesSent, sequenceNbr, splittedData));

            byte[] finalUnencryptedData = combineByteArrays(numberOfBytesSent, sequenceNbr, splittedData, hash);

            byte[] finalData = crypto.encrypt(finalUnencryptedData);

            send(finalData);
            sendSequenceNumber++;
            if(sendSequenceNumber == Integer.MAX_VALUE) {
                System.out.println("The session is to be renewed since all sequence numbers have been used up.");
                System.exit(0);
            }
        }
    }

    /*
    Use this method to receive actual/raw data
     */
    public static byte[] receiveData() throws  Exception{
        byte[] receivedData = receive();

        if(receivedData.length != MAX_PAYLOAD_DATA_IN_BYTES) {
            System.out.println("PACKET TOO SMALL, EXPECTED 48 BYTES");
            return null;
        }
        byte[] unencryptedData = crypto.decrypt(receivedData);
        int bytesSent = unencryptedData[0];
        unencryptedData = byteSplitter(unencryptedData, 0, bytesSent + BYTES_SENT_INDICATOR_SIZE_IN_BYTES);

        byte[] hash = byteSplitter(unencryptedData, BYTES_SENT_INDICATOR_SIZE_IN_BYTES + bytesSent - HASH_SIZE_IN_BYTES, BYTES_SENT_INDICATOR_SIZE_IN_BYTES + bytesSent);

        if(validateHash(byteSplitter(unencryptedData, 0, BYTES_SENT_INDICATOR_SIZE_IN_BYTES + bytesSent - HASH_SIZE_IN_BYTES), hash)) {
            System.out.println("Hash is valid");

            byte[] sequenceNbr = byteSplitter(unencryptedData, BYTES_SENT_INDICATOR_SIZE_IN_BYTES, BYTES_SENT_INDICATOR_SIZE_IN_BYTES + SEQUENCE_NUMBER_SIZE_IN_BYTES);
            int sequenceNbrInt = ByteBuffer.wrap(sequenceNbr).getInt();
            if(receiveSequenceNumber == sequenceNbrInt) {
                System.out.println("Sequence number is valid (" + sequenceNbrInt + ")");
                receiveSequenceNumber++;
                if(receiveSequenceNumber == Integer.MAX_VALUE) {
                    System.out.println("The session is to be renewed since all sequence numbers have been used up.");
                    System.exit(0);
                }
                String message = new String(byteSplitter(unencryptedData, BYTES_SENT_INDICATOR_SIZE_IN_BYTES + SEQUENCE_NUMBER_SIZE_IN_BYTES, BYTES_SENT_INDICATOR_SIZE_IN_BYTES + bytesSent - HASH_SIZE_IN_BYTES));
                System.out.println("Message received: " + message);
            } else {
                System.out.println("SEQUENCE NUMBER IS NOT VALID");
            }

            return receivedData;
        } else {
            System.out.println("INVALID HASH DETECTED");
            return null;
        }
    }

    public static void send(byte[] data) throws IOException {
        if(data.length > MAX_PACKET_SIZE) {
            System.out.println("PACKET TOO BIG");
            System.exit(0);
        }

        DatagramPacket sendPacket = new DatagramPacket(data, data.length, IPAddress, CACHE_PORT);
        System.out.println("Sending data to cache");
        clientSocket.send(sendPacket);
    }

    public static byte[] receive() throws IOException {
        byte[] data = new byte[MAX_PACKET_SIZE];
        DatagramPacket receivePacket = new DatagramPacket(data, data.length);
        clientSocket.receive(receivePacket);

        return byteSplitter(data, 0, receivePacket.getLength());
    }

    public static  byte[] byteSplitter(byte[] data, int start, int end) {
        byte[] result = new byte[end - start];
        for(int i = 0; i < end - start; i++) {
            result[i] = data[start + i];
        }
        return result;
    }

    private static byte[] combineByteArrays(byte[]... arrays) {
        int size = 0;
        for(int i = 0; i < arrays.length; i++) {
            size+= arrays[i].length;
        }
        byte[] result = new byte[size];
        int index = 0;
        for(int i = 0; i < arrays.length; i++) {
            byte[] part = arrays[i];
            for(int k = 0; k < part.length; k++) {
                result[index] = part[k];
                index++;
            }
        }
        return result;
    }

    private static byte[] hashData(byte[] data) throws NoSuchAlgorithmException {
        // Register the BouncyCastleProvider with the Security Manager
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        return messageDigest.digest(data);
    }

    private static boolean validateHash(byte[] data, byte[] hash) throws NoSuchAlgorithmException{
        byte[] calculatedHash = hashData(data);
        for(int i = 0; i < hash.length; i++) {
            if(calculatedHash[i] != hash[i]) {
                return false;
            }
        }
        return true;
    }
}
