package server;

import org.bouncycastle.util.encoders.Hex;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class KeyExchange {
    private final int BASE_SIZE_IN_BYTES = 4;
    private final int PRIME_SIZE_IN_BYTES = 16;
    private final int SECRET_SIZE_IN_BYTES = 4;

    private byte[] symmetricKey;

    public KeyExchange() {

    }

    public void handshake() throws IOException {
        System.out.println("Receiving base and prime");
        byte[] baseAndPrime = Main.receive();
        BigInteger base = new BigInteger(byteSplitter(baseAndPrime, 0, BASE_SIZE_IN_BYTES));
        BigInteger prime = new BigInteger(byteSplitter(baseAndPrime, BASE_SIZE_IN_BYTES, PRIME_SIZE_IN_BYTES + BASE_SIZE_IN_BYTES));
        byte[] test = byteSplitter(baseAndPrime, BASE_SIZE_IN_BYTES, PRIME_SIZE_IN_BYTES + BASE_SIZE_IN_BYTES);
        System.out.println("Base: " + base + "\nPrime: "+ prime);

        BigInteger secret = generateSecretInteger();
        System.out.println("Generated secret (b): " + secret);
        BigInteger part1 = part1Calculation(base, prime, secret);
        System.out.println("Sending B = (g^b mod p): " + part1);
        Main.send(part1.toByteArray());

        BigInteger otherBase = new BigInteger(Main.receive());
        System.out.println("Receiving B = (g^a mod p): " + otherBase);

        BigInteger sharedSecret = part2Calculation(otherBase, prime, secret);
        System.out.println("Shared secret (A^b mod p): " + sharedSecret);

        symmetricKey = generateSymmetricKey(sharedSecret);
    }

    public byte[] getSymmetricKey(){
        return symmetricKey;
    }

    // a or b
    private BigInteger generateSecretInteger() {
        while(true) {
            SecureRandom secureRandom = new SecureRandom();
            byte[] container = new byte[SECRET_SIZE_IN_BYTES];
            secureRandom.nextBytes(container);
            BigInteger bigInteger = new BigInteger(container);
            if(BigInteger.valueOf(65000).compareTo(bigInteger) < 0) {
                return bigInteger;
            }
        }
    }

    // g^a mod p or g^b mod p returns A or B
    private BigInteger part1Calculation(BigInteger base, BigInteger prime, BigInteger secret) {
        return base.modPow(secret, prime);
    }

    // B^a mod p or A^b mod p returns shared secret
    private BigInteger part2Calculation(BigInteger otherBase, BigInteger prime, BigInteger secret) {
        return otherBase.modPow(secret, prime);
    }

    private byte[] generateSymmetricKey(BigInteger sharedSecret) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(sharedSecret.toByteArray());
            String sha256hex = new String(Hex.encode(hash));
            System.out.println("Generated symmetric key: " + sha256hex);
            return hash;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    private byte[] combineByteArrays(byte[]... arrays) {
        int size = 0;
        for(int i = 0; i < arrays.length; i++) {
            size+= arrays[i].length;
        }
        byte[] result = new byte[size];
        int index = 0;
        for(int i = 0; i < arrays.length; i++) {
            byte[] part = arrays[i];
            for(int k = 0; k < part.length; k++) {
                result[index] = part[k];
                index++;
            }
        }
        return result;
    }

    private byte[] byteSplitter(byte[] data, int start, int end) {
        byte[] result = new byte[end - start];
        for(int i = 0; i < end - start; i++) {
            result[i] = data[start + i];
        }
        return result;
    }


}
